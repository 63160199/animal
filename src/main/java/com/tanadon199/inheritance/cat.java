/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.inheritance;

/**
 *
 * @author Kitty
 */
public class Cat extends Animal {
    public Cat (String name , String color){
        super(name,4,color);
        System.out.println("Cat Created");
    }
    @Override
    public void speak(){
        System.out.println("Cat Name "+this.name+" Speak "+"Maewooo!!!");
    }
    @Override
    public void walk(){
        System.out.println("Cat Name "+this.name+" Color "+this.color+" Walk by "+this.NumberOfLegs+" Legs");
        Separator();
    }
     public static void Separator() {
        System.out.println("..................................");
    }
}
