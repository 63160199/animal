/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.inheritance;

/**
 *
 * @author Kitty
 */
public class Animal {
    protected String name;
    protected int NumberOfLegs;
    protected String color;
    
    public Animal (String name, int NumberOfLegs, String color){
        System.out.println("Animal Created");
        this.name = name;
        this.NumberOfLegs=NumberOfLegs;
        this.color=color;
    }
    
    public void walk(){
        System.out.println("Animal Walk");
    }
    
    public void speak(){
        System.out.println("Animal Speak");
        System.out.println("name: "+this.name+" NumberOfLegs: "+this.NumberOfLegs+" Color: "+this.color);
    }
    
    public String getColor(){
        return this.color;
    }
}

