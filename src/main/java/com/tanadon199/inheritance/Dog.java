/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.inheritance;

/**
 *
 * @author Kitty
 */
public class Dog extends Animal {
    public Dog (String name  , String color){
        super(name,4,color);
        System.out.println("Dog Created");
    }
    
    @Override
    public void walk (){
        System.out.println("Dog name: "+this.name+" Color "+this.color+" walk with "+this.NumberOfLegs+" Legs");
        Separator();
    }
    
    @Override
    public void speak(){
        System.out.println("Dog name: "+this.name+" Speak Box Box!!!");
        
    }
     public static void Separator() {
        System.out.println("..................................");
    }
}

