/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.inheritance;

/**
 *
 * @author Kitty
 */
public class Duck extends Animal{
    private int NumberOfWings;
    public Duck (String name , String color){
        super(name,2,color);
        System.out.println("Duck Created");
    }
    
    public void Fly(){
        System.out.println("Duck name "+this.name+" Fly~~");
        Separator();
    }
    
    @Override
    public void speak(){
        System.out.println("Duck Name "+this.name+" Speak "+"Gaaaaapp Gaaaappp");
    }
    @Override
    public void walk(){
        System.out.println("Duck Name "+this.name+" Color "+this.color+" Walk by "+this.NumberOfLegs+" Legs");
        
    }
     public static void Separator() {
        System.out.println("..................................");
    }
}

